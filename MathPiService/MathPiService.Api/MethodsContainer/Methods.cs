﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MathPiService.Math;

namespace MathPiService.Api.MethodsContainer
{
    public class Methods : IMethods
    {
        public string GetCountedPi(int number)
        {
            string result = "";
            if (number < 15)
            {
                //для увеличения производительности используем встроенный функционал
                //точное значение
                result = System.Math.Round(System.Math.PI, number).ToString();
                return result;
            }
            // при вычислении пи для количества знаков после запятой больше 14 используем вычисления:
            // iterations = 3000 учтановлено опытным путем, про большей достаточно сильно забивается оперативка
            result = BigMath.GetPiAsString(number, 3000).Insert(1, ".");
            return result;
        }
    }
}