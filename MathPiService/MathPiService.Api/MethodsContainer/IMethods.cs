﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathPiService.Api.MethodsContainer
{
    public interface IMethods
    {
        string GetCountedPi(int number);
    }
}
