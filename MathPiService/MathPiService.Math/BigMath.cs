﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Web;

namespace MathPiService.Math
{
    public static class BigMath
    {
        // number = число знаков после запятой
        // iterations = точность вычислений, чем она больше, тем точнее вычисления
        public static string GetPiAsString(int number, int iterations)
        {
            return (16 * ArcTan1OverX(5, number).ElementAt(iterations)
                - 4 * ArcTan1OverX(239, number).ElementAt(iterations)).ToString();
        }

        //arctan(x) = x - x^3/3 + x^5/5 - x^7/7 + x^9/9 - ...
        public static IEnumerable<BigInteger> ArcTan1OverX(int x, int number)
        {
            var mag = BigInteger.Pow(10, number);
            var sum = BigInteger.Zero;
            bool sign = true;
            for (int i = 1; true; i += 2)
            {
                var current = mag / (BigInteger.Pow(x, i) * i);
                if (sign)
                {
                    sum += current;
                }
                else
                {
                    sum -= current;
                }
                yield return sum;
                sign = !sign;
            }
        }
    }
}