﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MathPiService.Api.MethodsContainer;
using MathPiService.Controllers;
using System.Net.Http;
using System.Web.Http;
using Shouldly;

namespace MathPiService.Test
{
    [TestClass]
    public class MathPiServiceControllerTest
    {
        [TestMethod]
        public void GetCountedPiTestWithTwoNumbersAfterComma()
        {
            Methods methods = new Methods();
            var controller = new MathPiServiceController(methods)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            // отправляем запрос методу
            string response = controller.GetCountedPi(2);

            // проверка
            response.ShouldBe("3.14");
        }

        [TestMethod]
        public void GetCountedPiTestWithZeroNumbersAfterComma()
        {
            Methods methods = new Methods();
            var controller = new MathPiServiceController(methods)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            // отправляем запрос методу
            string response = controller.GetCountedPi(0);

            // проверка
            response.ShouldBe("3");
        }

        [TestMethod]
        public void GetCountedPiTestWithTwentyNumbersAfterComma()
        {
            Methods methods = new Methods();
            var controller = new MathPiServiceController(methods)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            // отправляем запрос методу
            string response = controller.GetCountedPi(20);

            // проверка
            response.ShouldBe("3.14159265358979323860");
        }

        [TestMethod]
        public void GetCountedPiTestWithFourtyNumbersAfterComma()
        {
            Methods methods = new Methods();
            var controller = new MathPiServiceController(methods)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            // отправляем запрос методу
            string response = controller.GetCountedPi(14);

            // проверка
            response.ShouldBe("3.14159265358979");
        }

        [TestMethod]
        public void GetCountedPiTestWithFiftyNumbersAfterComma()
        {
            Methods methods = new Methods();
            var controller = new MathPiServiceController(methods)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            // отправляем запрос методу
            string response = controller.GetCountedPi(15);

            // проверка
            response.ShouldBe("3.141592653589812");
        }
    }
}