﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MathPiService.Api.MethodsContainer;
using Swashbuckle.Swagger.Annotations;

namespace MathPiService.Controllers
{
    public class MathPiServiceController : ApiController
    {
        /// <summary>
        /// Вычисление числа ПИ
        /// </summary>
        private readonly IMethods _methods;
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="methods"></param>
        public MathPiServiceController(IMethods methods)
        {
            _methods = methods;
        }

        /// <summary>
        /// Возвращает число пи до заданного знака после запятой
        /// </summary>
        /// <param name="number">Число знаков после запятой</param>
        /// <returns></returns>
        [HttpGet, ActionName("GetCountedPi")]
        public string GetCountedPi(int number)
        {
            //todo: реализовать проверку параметров, 
            //если будут какие либо ограничения
            return _methods.GetCountedPi(number).ToString();
        }
    }
}
