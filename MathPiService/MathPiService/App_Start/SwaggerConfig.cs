using Castle.Core.Internal;
using System.Web.Http;
using WebActivatorEx;
using MathPiService;
using Swashbuckle.Application;
using System.IO;
using WebGrease.Css.Extensions;
using System;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace MathPiService
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "MathPiService");
                        
                        // ����������� �������� ��� ������� �������
                        var binariesPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
                        Directory
                            .EnumerateFiles(binariesPath, "*.xml", SearchOption.AllDirectories)
                            .ForEach(c.IncludeXmlComments);
                    })
                .EnableSwaggerUi(c =>
                    {
                        // ������������� ������ �������
                        c.DocExpansion(DocExpansion.List);
                    });
        }
    }
}
